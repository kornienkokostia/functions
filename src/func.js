

const getSum = (str1, str2) => {
  if (!/^\d+$/.test(str1) && !/^\d+$/.test(str2)) {
    return false
  }
  if (str1 === '') {
    str1 = 0
  }
  if (str2 === '') {
    str2 = 0
  }
  let res = BigInt(str1) + BigInt(str2)
  return res.toString();
};



const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  listOfPosts.forEach(el => {
    if (el.author === authorName) {
      posts++
    }
    if (el.hasOwnProperty('comments')) {
      for (const el2 of el.comments) {
        if (el2.author === authorName) {
          comments++
        }
      }
    }
    
  });
  return `Post:${posts},comments:${comments}`
};


 



const tickets=(people)=> {
  let count25 = 0;
  let count50 = 0;
  for (const ppl of people) {
    switch (+ppl) {
      case 25:
        count25++
        break;
      case 50:
        count25--
        count50++
        break;
      case 100:
        if (count50 === 0 && count25 >= 3) {
          count25-=3
        }
        else{
          count25--
          count50--
        }
        break;
    }
    if(count25 < 0 || count50 < 0){
      return 'NO';
    }
  }    
  return 'YES';
};

console.log(tickets([25, 25, 50])) // => 'YES'
console.log(tickets([25, 100]))    // => 'NO'
console.log(tickets([25, 25, 50, 100])) // 'YES'
console.log(tickets([25, 50, 100]))// 'NO'
console.log(tickets(['25', '25', '50', '100'])) // 'YES'
console.log(tickets(['25', '50', '100'])) // 'NO'

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
